#!/bin/dash
# If your distro doesn't come with dash, you probably shouldn't be using
# this script anyways.

ROOTCHECK="$(id -u)"

if [ "$ROOTCHECK" -ne "0" ]
then
	echo "This script needs to be run as root."
	exit
fi
apt-get update
apt-get upgrade -y
apt-get autoremove -y
apt-get autoclean
