# Debian Updater

## About
This is a dash shell script for updating Debian. It's nothing too terribly
big or important. It's a bit polished, but it's really nothing special.

## License
This project is under the Unlicense.

Basically, this means that this script is in the public domain. Feel free
to incorporate in your scripts however you choose.
